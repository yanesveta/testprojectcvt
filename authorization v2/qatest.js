window.onload = function() {
  ieCheck();
  
  let email = document.getElementById("login");
  let loginButton = document.getElementById("loginButton");
  let password = document.getElementById("password");
  let bottomClock = document.getElementById('bottomClock');
//для проверки имейла и пароля используем станрдартный api в html5
  loginButton.addEventListener("click", function(event) {
    if (email.validity.typeMismatch) {
      email.setCustomValidity("Это не e-mail");
    } else {
      email.setCustomValidity("");
    };
    if (password.validity.patternMismatch) {
      password.setCustomValidity("Пароль должен содержать не менее 6 символов, должны быть символы разных регистров, должны присутствовать числа, должны присутствовать спец. символы");
    } else {
      password.setCustomValidity("");
    };
  });
  //прикрепляем дни недели для календаря
  let weekday=new Array(7);
  weekday[0]="Воскресенье";
  weekday[1]="Понедельник";
  weekday[2]="Вторник";
  weekday[3]="Среда";
  weekday[4]="Четверг";
  weekday[5]="Пятница";
  weekday[6]="Суббота";

  setTimeout(function run() {
    let date = new Date();
    bottomClock.innerHTML = ("Сегодня " + weekday[date.getDay()] + "  " + date.getDate() + "." + (date.getMonth()+1)+ "." + date.getFullYear()+ "    " + checkTime(date.getHours()) + ":" + checkTime(date.getMinutes()));
    setTimeout(run, 300);
  }, 300);
//добавляем нули в показатели времени, чтобы приятнее выглядело
  function checkTime(i)
  {
  if (i<10)
  {
  i="0" + i;
  }
  return i;
  }
//выявляем IE
  function ieCheck()
  {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
  
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
      {
          alert("У вас неправильный браузер!");
      }
      return false;
  }
  }
  